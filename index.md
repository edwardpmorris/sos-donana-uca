---
title: '#SOSDonana: Towards a Safe Operating Space for the iconic Doñana wetlands'
subtitle: ''
font-import: 'https://fonts.googleapis.com/css?family=Fira+Sans'
font-family: 'Fira Sans'
author: 'Edward P. Morris (UCA-INMAR-CEIMAR)'
job:  'Gestión de Aguas Continentales, Máster de Conservación - UCA - 9th March 2017'
logo        : "UCA-ceimar-logo-480-198-transparent.png"
framework   : io2012        # {io2012, html5slides, shower, dzslides, ...}
highlighter : prettify      # {highlight.js, prettify, highlight}
hitheme     : tomorrow      # 
widgets     : []            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {selfcontained, standalone, draft}
knit        : slidify::knit2slides
---

## What is a wetland?

+ Land surface that regularly has inundated, or saturated, conditions ([Melton et al. 2013](https://dx.doi.org/10.5194/bg-10-753-2013)).
+ An ecosystem that arises when inundation by water produces soils dominated by anaerobic processes, which, in turn, forces the biota, particularly rooted plants, to adapt to flooding ([Keddy 2010](http://www.drpaulkeddy.com/wetlandecology2.html)).

<div style="position: absolute; left: 200px; top:250px; width:700px">
<pw><a href="http://www.miseagrant.umich.edu/lessons/lessons/by-broad-concept/earth-science/wetlands/"><img alt="wetland-coastal-marsh-diagram.png" width=100% src="assets/img/wetland-coastal-marsh-diagram.png"></a><br>"<a href="http://www.miseagrant.umich.edu/lessons/lessons/by-broad-concept/earth-science/wetlands/">Great Lakes Coastal Wetlands</a>" by Michigan Sea Grant.</pw>
</div>

---
## What is a wetland?
####  United States Code (16 U.S.C., Section 3801(a)(18))
+ Land that (A) has a predominance of hydric soils, (B) is inundated or saturated by surface or groundwater at a frequency and duration sufficient to support a prevalence of hydrophytic vegetation typically adapted for life in saturated soil conditions and (C) under normal circumstances supports a prevalence of such vegetation.

<div style="position: absolute; left: 180px; top:290px; width:700px">
<pw><a href="http://slr.stormsmart.org/sea-level-rise/"><img alt="marsh_diagram.png" width=100% src="assets/img/marsh-diagram.png"></a><br>"<a href="http://slr.stormsmart.org/sea-level-rise/">Wetland Classifications</a>" by  Warren Pinnacle Consulting Inc.</pw>
</div>

---
## What is a wetland?
#### Ramsar definition
+ Article 1.1: "...wetlands are areas of marsh, fen, peatland or water, whether natural or artificial, permanent or temporary, with water that is static or flowing, fresh, brackish or salt, including areas of marine water the depth of which at low tide does not exceed six metres."
+ Article 2.1: "[Wetlands] may incorporate riparian and coastal zones adjacent to the wetlands, and islands or bodies of marine water deeper than six metres at low tide lying within the wetlands."

<div style="position: absolute; left: 340px; top:350px; width:400px">
<pw><a href="https://ramsar.org"><img alt="ramsarengsml.png" width=100% src="assets/img/ramsarengsml.png"></a></pw>
</div>

---
## What is a wetland?

#### Inland wetlands 
Marshes, ponds, lakes, fens, rivers, floodplains, and swamps.

#### Coastal wetlands
Salt marshes, estuaries, mangroves, lagoons and even coral reefs.

#### Human-made wetlands
Fish ponds, rice paddies, and salt pans.

<div style="position: absolute; right: 20px; top:20px; width:150px">
<pw><a href="https://ramsar.org"><img alt="ramsarengsml.png" width=100% src="assets/img/ramsarengsml.png"></a></pw>
</div>


---
## Where are the worlds wetlands?

<div style="position: absolute; left: 120px; top:80px; width:850px">
<pw><a href="https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/use/worldsoils/?cid=nrcs142p2_054021"><img alt="wetlands-map-usda.png" width=100% src="assets/img/wetlands-map-usda.png"></a><br>"<a href="https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/use/worldsoils/?cid=nrcs142p2_054021">Global Distribution of Wetlands Map</a>" sources: FAO-UNESCO, Soil Map of the World, digitized by ESRI. Soil climate map, USDA-NRCS, Soil Science Division, World Soil Resources, Washington D.C.</pw>
</div>



--- &twocol
## What is the value of a wetland?

*** =left

+ Flood control
+ Clean water
+ Food supply
+ Shoreline and storm protection
+ Cultural value

*** =right

+ Materials and Medicines
+ Recreation areas
+ Vital habitat
+ A refuge for migrating birds

<div style="position: absolute; left: 400px; top:350px; width:200px">
<pw><a href="http://wwf.panda.org/about_our_earth/about_freshwater/intro/value/"><img alt="WWF_logo.svg" width=100% src="assets/img/WWF_logo.svg"></a></pw>
</div>

--- &twocol
## What is the status of global wetlands?

*** =right
+ Long-term loss of natural wetlands between 54–57% since 1900 ([Davidson, 2014](http://dx.doi.org/10.1071/MF14173)).
+ Rate of loss in Europe has slowed, and in North America has remained low since the 1980s.
+ High in Asia; large-scale and rapid conversion of coastal and inland natural wetlands.
+ Big uncertainities in Africa, the Neotropics and Oceania.

<div style="position: absolute; left: 120px; top:120px; width:370px; font-size:0.7em">
<pw><a href="http://www.eorc.jaxa.jp/ALOS/en/kyoto/mangrovewatch.htm"><img alt="mangrove_fig1_borneo.png" width=100% src="assets/img/mangrove_fig1_borneo.png"></a><br>"<a href="http://www.eorc.jaxa.jp/ALOS/en/kyoto/mangrovewatch.htm">JERS-1 (1996) and ALOS PALSAR (2007, 2010) color composite of East Kalimantan (R:1996, B:2007, G:2010). The red areas have largely changed from mangrove forest in 1996 to aquaculture by 2007.</a>" by Global Mangrove Watch, JAXA.</pw>
</div>
 

--- &twocol
## What are the drivers?

<div style="position: absolute; left: 150px; top:150px; width:400px">
<pw><a href="http://www.ramsar.org/sites/default/files/documents/library/factsheet3_global_disappearing_act_0.pdf"><img alt="drivers-of-us-wetland-loss.png" width=100% src="assets/img/drivers-of-us-wetland-loss.png"></a><br>"<a href="http://www.ramsar.org/sites/default/files/documents/library/factsheet3_global_disappearing_act_0.pdf">Drivers of US wetland loss</a>" by Ramsar.</pw>
</div>

*** =right
+ Major changes in land use (agriculture, grazing animals and aquaculture).
+ Infrastructure development.
+ Water diversion and extraction.
+ Air and water pollution (excess nutrients).

--- &twocol
## The Safe Operating Space concept

*** =left
<pw><a href="http://www.nature.com/nature/journal/v461/n7263/fig_tab/461472a_F1.html"><img alt="a-safe-operating-space-for-humanity.jpg" width=500px src="assets/img/a-safe-operating-space-for-humanity.png"></a><br>"<a href="http://www.nature.com/nature/journal/v461/n7263/fig_tab/461472a_F1.html">Beyond the boundary</a>" by Rockstrom et al. 2009.</pw>

*** =right
+ First proposed by [Rockström et al. (2009)](https://dx.doi.org/10.1038/461472a).
+ Earth system’s processes act in a nonlinear way and are sensitive around threshold levels of key variables.
+ If these thresholds are crossed, severe and sometimes irreversible change can occur.
+ Makes sense to define the boundary of the safe space at a reasonable distance from these thresholds.


---
## The Safe Operating Space concept

<div style="position: absolute; left: 150px; top:130px; width:700px">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="sos-donana-figure-2-a.png" width=100% src="assets/img/sos-donana-figure-2-a.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">Response of an indicator of the state of an ecosystem</a>" by Martin Scheffer in Green et al. 2017.</pw>
</div>

--- 
## The Safe Operating Space concept

<div style="position: absolute; left: 230px; top:130px; width:580px">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="sos-donana-figure-2-b.png" width=100% src="assets/img/sos-donana-figure-2-b.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">The safe level for local stressors can decrease under a different climate</a>" by Martin Scheffer in Green et al. 2017.</pw>
</div>



--- &twocol
## The Safe Operating Space concept

<div style="position: absolute; right: 120px; top:130px; width:390px">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="sos-donana-figure-3.png" width=100% margin=auto src="assets/img/sos-donana-figure-3.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">The safe operating space illustrated in three dimensions</a>" by Martin Scheffer in Green et al. 2017.</pw>
</div>

*** =left

+ Ecological resilience to the effects of climate change may be enhanced by reducing the negative effects
of local stressors.
+ Controlling local stressors may allow the creation of a safe operating space in the face of a changing climate.

---width=750px
## Interacting stresses

<div style="position: absolute; left: 175px; top:100px; width:750px">
<h3>Allied attack: Climate change and eutrophication</h3>
<pw><a href="https://www.fba.org.uk/journals/index.php/IW/article/view/359"><img alt="fig-1-allied-attack-moss-et-al-2011.png" width=100% src="assets/img/fig-1-allied-attack-moss-et-al-2011.png"></a><br>"<a href="https://www.fba.org.uk/journals/index.php/IW/article/view/359">Some relationships now established that link climate change and eutrophication symptoms</a>" by Moss et al. 2011.</pw>
</div>


---
## Climate change and eutrophication

<div style="position: absolute; left: 50px; top:150px; width:970px;">
<pw><a href="https://dx.doi.org/10.1111/j.1365-2486.2011.02488.x"><img alt="fig-3-kosten-et-al-2012.png" width=100%; src="assets/img/fig-3-kosten-et-al-2012.png"></a><br>"<a href="https://dx.doi.org/10.1111/j.1365-2486.2011.02488.x">Percentage of cyanobacterial biovolume in phytoplankton communities as a function of water temperature and nutrients in 143 lakes along a climatic gradient in Europe and South America</a>" by Kosten et al. 2012.</pw>
</div>


--- &twocol
## Climate change and water extraction

<div style="position: absolute; left: 100px; top:130px; width:400px;">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="sos-donana-figure-4.png" width=100% margin=auto src="assets/img/sos-donana-figure-4.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">Summary of the synergetic effects of climate change and water extraction</a>" by Paloma Alcorlo in Green et al. 2017.</pw>
</div>

*** =right
+ Climate change will affect precipitation and evapo-transpiration (temperature, wind).
+ Initially there maybe increased precipitation in higher latitudes, but for wetlands this is likely to be offset by increased evaporation ([Melton et al. 2013](https://dx.doi.org/10.5194/bg-10-753-2013)). 
+ Less water availability (in dry periods) means more diversion and extraction, and potentially less for wetlands.

---
## Nutrients and land/water management

<div style="position: absolute; left: 250px; top:300px; width:600px;">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="nutrients-extraction.png" width=100% margin=auto src="assets/img/nutrients-extraction.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">Synergetic effects of nutrient loading and water extraction</a>" by Edward P. Morris.</pw>
</div>

+ With same nutrient loading a lower volume of water implies higher concentrations.
<br><br>
+ Although, with same water volume land management may affect nutrient loading resulting in higher concentrations.

--- .segue bg:url(assets/img/rro9291.jpg);background-size:cover;
## SOS Doñana

--- 
## Paleogeography eastern Gulf of Cádiz

<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf"><img alt="gulf_cadiz_paleogeography.png" width=100% margin=auto src="assets/img/gulf_cadiz_paleogeography.png"></a><br>"<a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf">Paelogeography eastern gulf of Cádiz. Adapted from Gracia et al. (2005) Geomorphology of the South-Atlantic Spanish Coast, SEG</a>" by Edward P. Morris.</pw>
</div>

--- 
## Paleogeography eastern Gulf of Cádiz

<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf"><img alt="gulf_cadiz_paleogeography.png" width=100% margin=auto src="assets/img/gulf_cadiz_paleogeography.png"></a><br>"<a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf">Paelogeography eastern gulf of Cádiz. Adapted from Gracia et al. (2005) Geomorphology of the South-Atlantic Spanish Coast, SEG</a>" by Edward P. Morris.</pw>
</div>
<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf"><img alt="gulf_cadiz_paleogeography_6500BC_water.png" width=100% margin=auto src="assets/img/gulf_cadiz_paleogeography_6500BC_water.png"></a></pw>
</div>
<div style="position:absolute;left:400px;top:330px;width:100px;color:white;">
<pw>Shallow bay</pw>
</div>
<div style="position: absolute; right: 250px; top:150px; width:150px;">
<h3>~6500 BC</h3>
</div>

--- 
## Paleogeography eastern Gulf of Cádiz

<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf"><img alt="gulf_cadiz_paleogeography.png" width=100% margin=auto src="assets/img/gulf_cadiz_paleogeography.png"></a><br>"<a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf">Paelogeography eastern gulf of Cádiz. Adapted from Gracia et al. (2005) Geomorphology of the South-Atlantic Spanish Coast, SEG</a>" by Edward P. Morris.</pw>
</div>
<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.geomorfologia.es/sites/default/files/A4%20C%C3%A1diz.pdf"><img alt="gulf_cadiz_paleogeography_500BC_sandspits.png" width=100% margin=auto src="assets/img/gulf_cadiz_paleogeography_500BC_sandspits.png"></a></pw>
</div>
<div style="position:absolute;left:500px;top:330px;width:100px;color:black;">
<pw>Estuarine infilling</pw>
</div>
<div style="position:absolute;left:300px;top:330px;width:100px;color:white;">
<pw>Growing coastal spits</pw>
</div>
<div style="position: absolute; right: 250px; top:150px; width:150px;">
<h3>~500 BC</h3>
</div>

--- 
## Major changes in land use

<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.juntadeandalucia.es/medioambiente/site/rediam/menuitem.aedc2250f6db83cf8ca78ca731525ea0/?vgnextoid=784efa937370f210VgnVCM1000001325e50aRCRD"><img alt="egc-lc-56-07.gif" width=100% margin=auto src="assets/img/egc-lc-56-07.gif"></a><br>"<a href="http://www.juntadeandalucia.es/medioambiente/site/rediam/menuitem.aedc2250f6db83cf8ca78ca731525ea0/?vgnextoid=784efa937370f210VgnVCM1000001325e50aRCRD">Land use changes eastern gulf of Cádiz. From REDIAM 'Mapa de Usos y Coberturas Vegetales de Andalucía 1:25.000'</a>" by Edward P. Morris.</pw>
</div>

--- 
## Major changes in land use

+ In 1940 "Institute of National Colinisation" began converversion of large areas of the wetlands for agriculture, a process that further intensified in the 1950s with the building of dykes isolating the wetlands in the North ([Maranon et al 1989](http://digital.csic.es/bitstream/10261/11765/3/Las%20marismas%20del%20Guadalquivir.pdf)).

+ More than 80% of the original wetland surface area was transformed ([Maranon et al 1989](http://digital.csic.es/bitstream/10261/11765/3/Las%20marismas%20del%20Guadalquivir.pdf), [Mendez et al 2012](http://www.ecologyandsociety.org/vol17/iss1/art26/)).

+ More recently (~ 1998), as a result of the public outcry generated by the Aznalcollar mining spill, the remaining wetland of the Doñana area were completely isolated from the estuary using dykes and floodgates.

+ The Guadalquivir estuary is now composed of a periodically dredged main channel with a few tidal creeks without any significant intertidal zones (only ~5% of original tidal marshes are still intact), it has poor water quality in it's upper reaches and very high suspended sediment concentrations ([Ruiz et al. 2015](http://link.springer.com/chapter/10.1007/978-3-319-06305-8_8)).


--- 
## Infrastructure development

<div style="position: absolute; left: 200px; top:130px; width:700px;">
<pw><a href="http://www.juntadeandalucia.es/medioambiente/site/rediam/menuitem.f361184aaadba3cf8ca78ca731525ea0/?vgnextoid=802462d34f44a310VgnVCM2000000624e50aRCRD&lr=lang_es"><img alt="coastal_population_change_IMA2011.png" width=100% margin=auto src="assets/img/coastal_population_change_IMA2011.png"></a><br>"<a href="http://www.juntadeandalucia.es/medioambiente/site/rediam/menuitem.f361184aaadba3cf8ca78ca731525ea0/?vgnextoid=802462d34f44a310VgnVCM2000000624e50aRCRD&lr=lang_es">Coastal population change (1991-2011). From REDIAM 'Informe de Medio Ambiente 2011'</a>".</pw>
</div>



---
## Water diversion and extraction

<div style="position: absolute; left: 50px; top:130px; width:1000px;">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="fig6_sos_donana_green_et_al_2017_1024-468.png" width=100% margin=auto src="assets/img/fig6_sos_donana_green_et_al_2017_1024-468.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">Schematic drawing of the wetlands in Doñana National Park and major associated threats.</a>" by Paloma Alcorlo and Edward P. Morris in Green et al. 2015.</pw>
</div>

---
## Water diversion and extraction

+ Within the park’s catchment area, unlicensed green-
houses occupy more than 6000 ha and there are over 1000
illegal groundwater-extracting wells ([Green et al. 2015](https://dx.doi.org/10.1002%2Ffee.1459)). 
+ Flow rates entering the marsh from the most important stream (the Rocina) declined by 50% in two decades as a result of groundwater extraction
([Guardiola-Albert and Jackson 2011](https://dx.doi.org/10.1007/s13157-011-0205-4)). 
+ Extraction rates are 90 hm^3^ (cubic hectometers) per year on average, representing 45% of the total aquifer recharge ([Custodio et al. 2009](http://www.juntadeandalucia.es/medioambiente/site/portalweb/menuitem.7e1cf46ddf59bb227a9ebe205510e1ca?vgnextoid=d533ee9b29da6210VgnVCM1000001325e50aRCRD&vgnextchannel=e16040a06a5f4310VgnVCM1000001325e50aRCRD)).
+ Expansion of Matalascañas beach resort (created in the
1960s) exacerbates overextraction of groundwater.

---
## Air and water pollution (excess nutrients)

+ Agricultural runoff, intense urbanization, inadequate wastewater treatment, and extensive hydrological modifications have led to high nutrient loading in some areas of the wetlands (Serrano et al. 2006; Olías et al. 2008; Mendez et al. 2012; Espinar et al. 2015).

<div style="position: absolute; left: 200px; top:250px; width:700px;">
<pw><a href="https://digital.csic.es/handle/10261/110763"><img alt="p-in donana-espinar-2015.png" width=100% margin=auto src="assets/img/p-in donana-espinar-2015.png"></a><br>"<a href="https://digital.csic.es/handle/10261/110763">Increase in 
phosphorus (P) in the Doñana National Park marsh and entry streams.</a>" by by Espinar et al. 2015.</pw>
</div>

---
## Signs of stress?

+ Alien floating fern *Azolla filiculoides* recorded in region since 2001
([Espinar et al. 2015](https://digital.csic.es/handle/10261/110763)).

+ Toxins associated with cyanobacterial blooms (*Microcystis*, *Pseudanabaena*, and *Anabaena*) implicated in mass mortalities of fish and waterbirds, including globally threatened species (López-Rodas et al. 2008).

<div style="position: absolute; left: 400px; top:270px; width:500px;">
<pw><a href="http://last-ebd.blogspot.com.es/2014/10/entrevista-javier-bustamante-en-iagua.html"><img alt="Caballero_175.JPG" width=100% margin=auto src="assets/img/Caballero_175.JPG"></a><br>"<a href="http://last-ebd.blogspot.com.es/2014/10/entrevista-javier-bustamante-en-iagua.html">Javier Bustamante muestreando Azolla en Doñana</a>" by LAST-EBD.</pw>
</div>

---
## Signs of stress?

+ Guadalquivir estuary has very poor water quality ([Ruiz et al. 2015](http://link.springer.com/chapter/10.1007/978-3-319-06305-8_8))

<div style="position: absolute; left: 200px; top:170px; width:700px;">
<pw><a href="http://link.springer.com/chapter/10.1007/978-3-319-06305-8_8"><img alt="no3-pi-guadaulquivir-ruiz-et-al-2015.png" width=100% margin=auto src="assets/img/no3-pi-guadaulquivir-ruiz-et-al-2015.png"></a><br>"<a href="http://link.springer.com/chapter/10.1007/978-3-319-06305-8_8">Inorganic nitrogen and phosphorous concentartions in the Guadalquivir estuary</a>" by Ruiz et al. 205.</pw>
</div>

--- &twocol
## Signs of stress?

<div style="position: absolute; left: 50px; top:120px; width:500px;">
<pw><a href="http://spei.csic.es/"><img alt="drought-index_spei_donana_1901-2011.png" width=100% margin=auto src="assets/img/drought-index_spei_donana_1901-2011.png"></a><br>"<a href="http://spei.csic.es/">The Standardised Precipitation-Evapotranspiration Index between 1901 and 2011 for the Doñana region. Extracted from the Global SPEI database, hosted by digitalCSIC</a>" by Edward P. Morris.</pw>
</div>



*** =right

+ Tendency for more extreme dry events in the region?

--- &twocol
## Towards a SOS for Doñana

*** =right

+ As illustrated by the Doñana case study, wetlands can
be threatened by local stressors that – together with
the effects of a changing climate – collectively erode
ecosystem resilience. 
+ Although local or national agencies cannot single-handedly mitigate climate change, they have the option to build a safe operating space for
Doñana and other wetlands subject to changing conditions.

<div style="position: absolute; left: 100px; top:150px; width:400px;">
<pw><a href="https://dx.doi.org/10.1002%2Ffee.1459"><img alt="sos.png" width=100% margin=auto src="assets/img/sos.png"></a><br>"<a href="https://dx.doi.org/10.1002%2Ffee.1459">Is Doñana within a 'Safe Operating Space' in terms of land use changes, development, water availability and nutrient loading</a>" by Edward P. Morris.</pw>
</div>

---
## Acknowledgements

AJG was supported by a WIMEK grant for a research
stay at WUR. EPM was supported by a JAE DOCTORES
2010 contract funded by the European Union (European
Social Fund, ESF2007-2013) and the Spanish Ministry
for Economy and Competitiveness, as well as the EU
FP7 project FAST (grant 607131). Funding was also
provided by the European Union’s Horizon 2020 research
and innovation program under grant agreement number
641762 to the ECOPOTENTIAL project. C Perennou
provided helpful comments on earlier versions of the
manuscript. Maps were made using data from CHG
uadalquivir (www.chguadalquivir.es/ide) with QGIS
(www.qgis.org) and the Google Maps API.

